import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router';
import HelloWorld from '@/components/HelloWorld.vue';

// @ts-ignore
const routes: Array<RouteRecordRaw> = [
  {
    path: '',
    redirect: (_) => {
      return {path: '/home'};
    }
  },
  {
    path: '/home',
    name: 'HelloWorld',
    component: HelloWorld
  },
  {
    path: '/about',
    name: 'About',
    //使用import可以路由懒加载，如果不使用，太多组件一起加载会造成白屏
    component: () =>
      import(/* webpackChunkName: "About" */ '@/pages/About.vue')
  },
  {
    path: '/mock',
    name: 'Mock',
    component: () => import('@/pages/mock.vue')
  },
  {
    path: '/:currentPath(.*)*', // 路由未匹配到，进入这个
    redirect: (_) => {
      return {path: '/404'};
    }
  },
  {
    path: '/404',
    name: '404',
    //使用import可以路由懒加载，如果不使用，太多组件一起加载会造成白屏
    component: () =>
      import(/* webpackChunkName: "About" */ '@/pages/error/404.vue')
  }
];
const router = createRouter({
  history: createWebHistory(''),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return {
      el: '#app',
      top: 0,
      behavior: 'smooth'
    };
  }
});
export default router;
